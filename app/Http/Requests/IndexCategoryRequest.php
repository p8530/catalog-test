<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexCategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => ['nullable', 'max:255'],
            'category_id'   => ['nullable', 'uuid', 'exists:App\Models\Category,id'],
            'category_name' => ['nullable', 'max:255'],
            'prices'        => ['nullable', 'regex:/^\d+,\d+$/u'],
            'is_published'  => ['nullable', 'boolean'],
        ];
    }
}
