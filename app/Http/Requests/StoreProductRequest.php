<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => ['required', 'max:255'],
            'price'        => ['required', 'integer', 'min:0'],
            'is_published' => ['nullable', 'boolean'],
            'categories'   => ['required', 'array', 'min:2', 'max:10'],
            'categories.*' => ['required', 'uuid', 'distinct', 'exists:App\Models\Category,id'],
        ];
    }
}
