<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    public function store(StoreCategoryRequest $request)
    {
        return Category::query()
                       ->create($request->validated());
    }

    public function destroy(Category $category)
    {
        abort_if($category->products->isNotEmpty(), 500, 'The category is attached to the product.');

        if ($category->delete()) {
            return response(null, 204);
        }

        return response(null, 500);
    }
}
