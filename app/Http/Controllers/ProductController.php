<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexCategoryRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(IndexCategoryRequest $request)
    {
        $query = Product::query()
                        ->with('categories')
                        ->when($request->has('name'), fn ($builder) => $builder->where('name', 'ilike', '%' . $request->get('name') . '%'))
                        ->when($request->has('category_id'), fn ($builder) => $builder->whereHas('categories', fn ($builder) => $builder->where('id', $request->get('category_id'))))
                        ->when($request->has('category_name'), fn ($builder) => $builder->whereHas('categories', fn ($builder) => $builder->where('name', 'ilike', '%' . $request->get('category_name') . '%')))
                        ->when($request->has('is_published'), fn ($builder) => $builder->where('is_published', boolval($request->get('is_published'))))
                        ->when($request->has('prices'), function ($builder) use ($request) {
                            [$from, $to] = array_map('trim', explode(',', $request->get('prices')));
                            $builder->where('price', '>=', $from)
                                    ->where('price', '<=', $to);
                        })
                        ->latest();

        return $query->get();
    }

    public function store(StoreProductRequest $request)
    {
        /** @var Product $product */
        $product = Product::query()
                          ->create($request->validated());
        $product->categories()
                ->attach($request->get('categories'));
        $product->load('categories');

        return $product;
    }

    public function show($id)
    {
        return Product::query()
                      ->findOrFail($id);
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->update($request->validated());

        return $product;
    }

    public function destroy(Product $product)
    {
        if ($product->delete()) {
            return response(null, 204);
        }

        return response(null, 500);
    }
}
