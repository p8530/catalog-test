<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Model extends BaseModel
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $model) {
            if (! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });

        self::saved(function (self $model) {
            Cache::flush();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
