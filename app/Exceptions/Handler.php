<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
        });
    }

    public function render($request, $e)
    {
        if ($e instanceof ModelNotFoundException) {
            return $this->error($e->getMessage(), 404);
        }

        if ($e instanceof HttpException) {
            if ($e instanceof NotFoundHttpException) {
                return $this->error('Method not found.', $e->getStatusCode());
            }

            return $this->error($e->getMessage(), $e->getStatusCode());
        }

        if ($e instanceof AuthorizationException || $e instanceof TokenMismatchException) {
            return $this->error($e->getMessage(), 500);
        }

        return parent::render($request, $e);
    }

    protected function error(string $message, int $statusCode): JsonResponse
    {
        return response()->json([
            'error' => $message,
        ], $statusCode);
    }
}
