.PHONY: up
up:
	docker-compose --env-file=.env --file=.docker/env/local/docker-compose.yml up -d --build --remove-orphans

.PHONY: down
down:
	docker-compose --env-file=.env --file=.docker/env/local/docker-compose.yml down

.PHONY: console
console:
	docker-compose --env-file=.env --file=.docker/env/local/docker-compose.yml exec app sh

.PHONY: logs
logs:
	docker-compose --env-file=.env --file=.docker/env/local/docker-compose.yml logs


.PHONY: adminer
adminer:
	curl --location https://www.adminer.org/latest.php --output public/adminer.php
