<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('products', 'ProductController');

Route::resource('category', 'CategoryController')
     ->only([
         'store',
         'destroy',
     ]);
