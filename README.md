# Catalog Test

### Тестовое задание для PHP-разработчика.

* Используя фреймворк Laravel реализовать RESTful api.
* Реализовать сущности
    * Товары
    * Категории
    * Товар-Категория
* Реализовать выдачу данных в формате json по RESTful
    * Создание Товаров (у каждого товара может быть от 2х до 10 категорий)
        * [POST] http://localhost:8000/products
    * Редактирование Товаров
        * [PUT] http://localhost:8000/products/{product_id}
    * Удаление товаров (товар помечается как удаленный)
        * [DELETE] http://localhost:8000/products/{product_id}
    * Создание категорий
        * [POST] http://localhost:8000/category
    * Удаление категорий (вернуть ошибку если категория прикреплена к товару)
        * [DELETE] http://localhost:8000/category/{category_id}
    * Получение списка товаров
        * [GET] http://localhost:8000/products
        * Имя / по совпадению с именем
            * [GET] http://localhost:8000/products?name=Test
        * id категории
            * [GET] http://localhost:8000/products?category_id={category_id}
        * Название категории / по совпадению с категорией
            * [GET] http://localhost:8000/products?category_name=Test
        * Цена от - до
            * [GET] http://localhost:8000/products?prices=101,120
        * Опубликованные да / нет
            * [GET] http://localhost:8000/products?is_published=0
        * Не удаленные

Результат представить ссылкой на репозиторий.

! Важно, в репозиторий залить пустой каркас приложения, а затем с внесенными изменениями, чтобы можно было проследить diff.
