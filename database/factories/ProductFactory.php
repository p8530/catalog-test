<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'         => $this->faker->sentence(mt_rand(2, 5)),
            'is_published' => $this->faker->boolean(90),
            'price'        => $this->faker->numberBetween(50, 150),
        ];
    }
}
